#!/usr/bin/make -f

include /usr/share/cdbs/1/rules/autoreconf.mk
include /usr/share/cdbs/1/class/autotools.mk
include /usr/share/cdbs/1/rules/debhelper.mk

pkg = $(DEB_SOURCE_PACKAGE)
stem = $(patsubst lib%,%,$(pkg))
abi = 0
lib = lib$(stem)
libpkg = lib$(stem)-$(abi)
devpkg = lib$(stem)-dev

# resolve if release is experimental
EXP_RELEASE = $(filter experimental% UNRELEASED,$(shell dpkg-parsechangelog -S Distribution))

DEB_CONFIGURE_EXTRA_FLAGS += \
 --libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH) \
 --enable-endomorphism \
 --enable-module-recovery \
 $(if $(EXP_RELEASE),--enable-experimental)

# avoid accidentally linking against disabled experimental modules
#CDBS_BUILD_DEPENDS +=, default-jdk-headless | default-jdk, libguava-java
#DEB_CONFIGURE_EXTRA_FLAGS += --enable-jni
DEB_CONFIGURE_EXTRA_FLAGS += --disable-jni

DEB_MAKE_CHECK_TARGET = check

# relax symbols check when targeting experimental
export DPKG_GENSYMBOLS_CHECK_LEVEL=$(if $(EXP_RELEASE),0,1)

# Let d-shlibs calculate development package dependencies
#  and handle shared library install
binary-post-install/$(libpkg) binary-post-install/$(devpkg):: \
 debian/stamp-local-shlibs-$(lib)
debian/stamp-local-shlibs-$(lib): \
 binary-install/$(libpkg) \
 binary-install/$(devpkg)
	d-shlibmove --commit \
		--devunversioned \
		--exclude-la \
		--multiarch \
		--override 's/libgmp10-dev/libgmp-dev/' \
		--movedev "debian/tmp/usr/include/*" usr/include/ \
		--movedev "debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/*" \
			usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig \
		debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/$(lib).so
	touch $@
clean::
	rm -f debian/stamp-local-shlibs-$(lib)

# track symbols using pkgkde-symbolshelper
include /usr/share/pkg-kde-tools/makefiles/1/cdbs/symbolshelper.mk
